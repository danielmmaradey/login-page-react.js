import React, { Component } from 'react';
import { Redirect, Route } from 'react-router-dom';
import './DefaultLayout.scss';

import Header from '../../components/Header/Header';

import ReactTimeout from 'react-timeout';

class DefaultLayout extends Component {

    state = {
        opacity: false,
        opacityNone: false
    }

    componentWillMount = () => {
        this.changeOpacity();
    };

    opacity = () => {
        this.setState({
            opacity: true
        })
    }

    opacityNone = () => {
        this.setState({
            opacityNone: true
        })
    }

    changeOpacity = () => {
        this.props.setTimeout(this.opacity, 500)
        this.props.setTimeout(this.opacityNone, 800)
    }

    // Get user value

    getUsername = (user) => {
        return (
            <h1>Welcome {user}</h1>
        )
    }

    render() {
        const user = sessionStorage.getItem('session')
        const { opacity, opacityNone } = this.state;


        const getuser = this.getUsername(user);

        if (user === null || user === undefined) {
            return (
                <Route render={() => <Redirect to="/login" />} />
            )
        }

        return (
            <div className="App">
                <Header logout={this.state.Logout} />
                <div className="main">
                    <main className="app-content">
                        <div className="space-8">
                            {getuser}
                        </div>
                    </main>
                </div>
                <div id="s1" className={`ss ${opacity ? "opacity-l" : ""}`}>
                    <div id="s2" className={`sss ${opacityNone ? "opacity-g" : ""}`}></div>
                </div>
            </div>
        );
    }
}

export default ReactTimeout(DefaultLayout);