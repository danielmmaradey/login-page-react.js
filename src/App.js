import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// containers

import  Login  from './containers/Login/Login';
import  Page404  from './containers/Page404/Page404';
import  DefaultLayout  from './containers/DefaultLayout/DefaultLayout';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/login" name="Login Page" component={Login} />
          <Route exact path="/404" name="Page 404" component={Page404} />
          <Route path="/" name="Home" component={DefaultLayout} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
